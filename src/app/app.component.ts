import {Component} from '@angular/core';
import OrgChart from '@balkangraph/orgchart.js';
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	// horseJson = [
	// 	{id: 1, name: 'Horse 1', img: 'assets/images/horse-1.jpg'},
	// 	{id: 2, pid: 1, name: 'Horse 11', img: 'assets/images/horse-2.jpg'},
	// 	{id: 3, pid: 1, name: 'Horse 12', img: 'assets/images/horse-3.jpg'},
	// 	{id: 4, pid: 2, name: 'Horse 21', img: 'assets/images/horse-4.jpg'},
	// 	{id: 5, pid: 2, name: 'Horse 22', img: 'assets/images/horse-5.jpg'},
	// 	{id: 6, pid: 3, name: 'Horse 31', img: 'assets/images/horse-6.jpg'},
	// 	{id: 7, pid: 3, name: 'Horse 33', img: 'assets/images/horse-7.jpg'},
	// ];

	constructor() {}

	ngOnInit() {
		var chart2 = new OrgChart(document.getElementById('pedigree2'), {
			template: 'olivia',
			nodeBinding: {
				field_0: 'name',
				img_0: 'img',
				// html: 'html',
			},
			orientation: OrgChart.orientation.left, // horizontal tree
			enableSearch: false, // serach specific node disable
			mouseScrool: OrgChart.action.none, //zoom in out none
			nodeMouseClick: OrgChart.action.none, // to avoid opening edit form pop up
			nodeMenu: {
				edit: {
					icon: OrgChart.icon.edit(18, 18, '#c1c8cc'),
					text: '',
				},
			},
			nodes: [
				{
					id: 1,
					name: 'Horse 1',
					img: 'assets/images/horse-1.jpg',
					// html:
					// 	'<input type="text" value="abc" style="padding: 6px; border-radius: 6px;position:relative;top:34px;left:80px;width:100px;" />',
				},
				{
					id: 2,
					pid: 1,
					name: 'Horse 11',
					img: 'assets/images/horse-2.jpg',
					// html:
					// 	'<input type="text" value="abc" style="padding: 6px; border-radius: 6px;position:relative;top:34px;left:80px;width:100px;" />',
				},
				{
					id: 3,
					pid: 1,
					name: 'Horse 12',
					img: 'assets/images/horse-3.jpg',
					// html:
					// 	'<input type="text" value="abc" style="padding: 6px; border-radius: 6px;position:relative;top:34px;left:80px;width:100px;" />',
				},
				{
					id: 4,
					pid: 2,
					name: 'Horse 21',
					img: 'assets/images/horse-4.jpg',
					// html:
					// 	'<input type="text" value="abc" style="padding: 6px; border-radius: 6px;position:relative;top:34px;left:80px;width:100px;" />',
				},
				{
					id: 5,
					pid: 2,
					name: 'Horse 22',
					img: 'assets/images/horse-5.jpg',
					// html:
					// 	'<input type="text" value="abc" style="padding: 6px; border-radius: 6px;position:relative;top:34px;left:80px;width:100px;" />',
				},
				{
					id: 6,
					pid: 3,
					name: 'Horse 31',
					img: 'assets/images/horse-6.jpg',
					// html:
					// 	'<input type="text" value="abc" style="padding: 6px; border-radius: 6px;position:relative;top:34px;left:80px;width:100px;" />',
				},
				{
					id: 7,
					pid: 3,
					name: 'Horse 33',
					img: 'assets/images/horse-7.jpg',
					// html:
					// 	'<input type="text" value="abc" style="padding: 6px; border-radius: 6px;position:relative;top:34px;left:80px;width:100px;" />',
				},
			],
		});
		OrgChart.templates.olivia.html =
			'<foreignobject class="node" x="20" y="10" width="200" height="100">{val}</foreignobject>';
		OrgChart.templates.olivia.link =
			'<path stroke-linejoin="round" stroke="#aeaeae" stroke-width="1px" fill="none" d="{edge}" />'; // for changing stroke style
		// option for stoke - d="{curve/edge/rounded}"

		OrgChart.templates.olivia.plus = ''; // for disabling collapse feature
		OrgChart.templates.olivia.minus = ''; //for disabling  expand feature

		// chart2.load();

		chart2.on('redraw', function () {
			var filedElements = document.querySelectorAll('.field_0');
			console.log('abcabc');
		});

		//---------------------------------------------------------------------------------------------------------------------

		// 	var chart3 = new OrgChart(document.getElementById('pedigree3'), {
		// 		template: 'polina',
		// 		nodeBinding: {
		// 			field_0: 'name',
		// 			img_0: 'img',
		// 		},
		// 		orientation: OrgChart.orientation.left, // horizontal tree
		// 		enableSearch: false, // serach specific node disable
		// 		mouseScrool: OrgChart.action.none, //zoom in out none,
		// 		nodeMouseClick: OrgChart.action.none, // to avoid opening edit form pop up
		// 	});

		// 	chart3.load(this.horseJson);
		// }
		// abc() {
		// 	// debugger;
		// 	// this.horseJson.push({id: 11, name: 'horse abc', img: 'assets/images/horse-8.jpg', pid: 1});
		// 	console.log(this.horseJson);

		//---------------------------------------
		// var chart1 = new OrgChart(document.getElementById('pedigree1'), {
		// 	template: 'ula',
		// 	nodeBinding: {
		// 		field_0: 'name',
		// 		img_0: 'img',
		// 	},
		// 	orientation: OrgChart.orientation.left, // horizontal tree
		// 	enableSearch: false, // serach specific node disable
		// 	mouseScrool: OrgChart.action.none, //zoom in out none
		// 	// scaleInitial: OrgChart.match.width,
		// });

		// chart1.load(this.horseJson);

		//-----------------------------------------------------------------------------------------------------------------
	}
}
